# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '.\CalcularValveRetencion.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_CalcularValveRetencion(object):
    def setupUi(self, CalcularValveRetencion):
        CalcularValveRetencion.setObjectName("CalcularValveRetencion")
        CalcularValveRetencion.resize(542, 291)
        CalcularValveRetencion.setStyleSheet("background-color: rgb(188, 255, 168);")
        self.label_3 = QtWidgets.QLabel(CalcularValveRetencion)
        self.label_3.setGeometry(QtCore.QRect(150, 0, 291, 61))
        font = QtGui.QFont()
        font.setFamily("Century Gothic")
        font.setPointSize(12)
        font.setBold(True)
        font.setItalic(False)
        font.setUnderline(True)
        font.setWeight(75)
        self.label_3.setFont(font)
        self.label_3.setStyleSheet("font: 75 bold 12pt \"Century Gothic\";\n"
"text-decoration: underline;")
        self.label_3.setObjectName("label_3")
        self.lineEdit_4 = QtWidgets.QLineEdit(CalcularValveRetencion)
        self.lineEdit_4.setGeometry(QtCore.QRect(80, 20, 31, 22))
        self.lineEdit_4.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.lineEdit_4.setObjectName("lineEdit_4")
        self.pushButton_5 = QtWidgets.QPushButton(CalcularValveRetencion)
        self.pushButton_5.setGeometry(QtCore.QRect(470, 10, 31, 28))
        self.pushButton_5.setStyleSheet("background-color: rgb(55, 159, 147);\n"
"font: 75 10pt \"MS Shell Dlg 2\";")
        self.pushButton_5.setObjectName("pushButton_5")
        self.label_6 = QtWidgets.QLabel(CalcularValveRetencion)
        self.label_6.setGeometry(QtCore.QRect(230, 70, 101, 20))
        font = QtGui.QFont()
        font.setFamily("Calibri")
        font.setPointSize(10)
        self.label_6.setFont(font)
        self.label_6.setObjectName("label_6")
        self.lineEdit_5 = QtWidgets.QLineEdit(CalcularValveRetencion)
        self.lineEdit_5.setGeometry(QtCore.QRect(320, 70, 61, 22))
        self.lineEdit_5.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.lineEdit_5.setObjectName("lineEdit_5")
        self.pushButton_11 = QtWidgets.QPushButton(CalcularValveRetencion)
        self.pushButton_11.setGeometry(QtCore.QRect(400, 90, 61, 21))
        self.pushButton_11.setStyleSheet("color: rgb(255, 255, 255);\n"
"background-color: rgb(82, 52, 62);")
        self.pushButton_11.setObjectName("pushButton_11")
        self.label_11 = QtWidgets.QLabel(CalcularValveRetencion)
        self.label_11.setGeometry(QtCore.QRect(250, 200, 51, 22))
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.label_11.setFont(font)
        self.label_11.setAlignment(QtCore.Qt.AlignCenter)
        self.label_11.setObjectName("label_11")
        self.lineEdit_6 = QtWidgets.QLineEdit(CalcularValveRetencion)
        self.lineEdit_6.setGeometry(QtCore.QRect(300, 200, 61, 22))
        self.lineEdit_6.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.lineEdit_6.setObjectName("lineEdit_6")
        self.label_7 = QtWidgets.QLabel(CalcularValveRetencion)
        self.label_7.setGeometry(QtCore.QRect(260, 100, 31, 21))
        font = QtGui.QFont()
        font.setFamily("Calibri")
        font.setPointSize(10)
        self.label_7.setFont(font)
        self.label_7.setObjectName("label_7")
        self.lineEdit_7 = QtWidgets.QLineEdit(CalcularValveRetencion)
        self.lineEdit_7.setGeometry(QtCore.QRect(320, 100, 61, 22))
        self.lineEdit_7.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.lineEdit_7.setObjectName("lineEdit_7")
        self.lineEdit_8 = QtWidgets.QLineEdit(CalcularValveRetencion)
        self.lineEdit_8.setGeometry(QtCore.QRect(320, 130, 61, 22))
        self.lineEdit_8.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.lineEdit_8.setObjectName("lineEdit_8")
        self.label_8 = QtWidgets.QLabel(CalcularValveRetencion)
        self.label_8.setGeometry(QtCore.QRect(250, 130, 51, 20))
        font = QtGui.QFont()
        font.setFamily("Calibri")
        font.setPointSize(10)
        self.label_8.setFont(font)
        self.label_8.setObjectName("label_8")
        self.label_5 = QtWidgets.QLabel(CalcularValveRetencion)
        self.label_5.setGeometry(QtCore.QRect(20, 20, 51, 16))
        font = QtGui.QFont()
        font.setFamily("Calibri")
        font.setPointSize(10)
        self.label_5.setFont(font)
        self.label_5.setObjectName("label_5")
        self.label_9 = QtWidgets.QLabel(CalcularValveRetencion)
        self.label_9.setGeometry(QtCore.QRect(30, 100, 167, 87))
        self.label_9.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.label_9.setObjectName("label_9")
        self.pushButton_17 = QtWidgets.QPushButton(CalcularValveRetencion)
        self.pushButton_17.setGeometry(QtCore.QRect(310, 250, 91, 28))
        self.pushButton_17.setStyleSheet("color: rgb(255, 255, 255);\n"
"background-color: rgb(82, 52, 62);")
        self.pushButton_17.setObjectName("pushButton_17")
        self.pushButton_18 = QtWidgets.QPushButton(CalcularValveRetencion)
        self.pushButton_18.setGeometry(QtCore.QRect(200, 250, 91, 28))
        self.pushButton_18.setStyleSheet("color: rgb(255, 255, 255);\n"
"background-color: rgb(82, 52, 62);")
        self.pushButton_18.setObjectName("pushButton_18")

        self.retranslateUi(CalcularValveRetencion)
        QtCore.QMetaObject.connectSlotsByName(CalcularValveRetencion)

    def retranslateUi(self, CalcularValveRetencion):
        _translate = QtCore.QCoreApplication.translate
        CalcularValveRetencion.setWindowTitle(_translate("CalcularValveRetencion", "Dialog"))
        self.label_3.setText(_translate("CalcularValveRetencion", "<html><head/><body><p align=\"center\">Calcular Válvula </p><p align=\"center\">Retención y Cierre</p></body></html>"))
        self.pushButton_5.setText(_translate("CalcularValveRetencion", "?"))
        self.label_6.setText(_translate("CalcularValveRetencion", "Ft Material"))
        self.pushButton_11.setText(_translate("CalcularValveRetencion", "Calcular"))
        self.label_11.setText(_translate("CalcularValveRetencion", "K"))
        self.label_7.setText(_translate("CalcularValveRetencion", "∅"))
        self.label_8.setText(_translate("CalcularValveRetencion", "D1/D2"))
        self.label_5.setText(_translate("CalcularValveRetencion", "Válvula"))
        self.label_9.setText(_translate("CalcularValveRetencion", "<html><head/><body><p><img src=\":/Valve/ValveRetencionIcon.png\"/></p></body></html>"))
        self.pushButton_17.setText(_translate("CalcularValveRetencion", "Añadir"))
        self.pushButton_18.setText(_translate("CalcularValveRetencion", "Cancelar"))

import mainqrc_rc

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    CalcularValveRetencion = QtWidgets.QDialog()
    ui = Ui_CalcularValveRetencion()
    ui.setupUi(CalcularValveRetencion)
    CalcularValveRetencion.show()
    sys.exit(app.exec_())

