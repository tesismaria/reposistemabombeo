# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '.\CalcularValveGlobo.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_CalcularValveGlobo(object):
    def setupUi(self, CalcularValveGlobo):
        CalcularValveGlobo.setObjectName("CalcularValveGlobo")
        CalcularValveGlobo.resize(542, 291)
        CalcularValveGlobo.setStyleSheet("background-color: rgb(188, 255, 168);")
        self.label_3 = QtWidgets.QLabel(CalcularValveGlobo)
        self.label_3.setGeometry(QtCore.QRect(150, 0, 261, 41))
        font = QtGui.QFont()
        font.setFamily("Century Gothic")
        font.setPointSize(12)
        font.setBold(True)
        font.setItalic(False)
        font.setUnderline(True)
        font.setWeight(75)
        self.label_3.setFont(font)
        self.label_3.setStyleSheet("font: 75 bold 12pt \"Century Gothic\";\n"
"text-decoration: underline;")
        self.label_3.setObjectName("label_3")
        self.lineEdit_4 = QtWidgets.QLineEdit(CalcularValveGlobo)
        self.lineEdit_4.setGeometry(QtCore.QRect(80, 20, 31, 22))
        self.lineEdit_4.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.lineEdit_4.setObjectName("lineEdit_4")
        self.pushButton_5 = QtWidgets.QPushButton(CalcularValveGlobo)
        self.pushButton_5.setGeometry(QtCore.QRect(470, 10, 31, 28))
        self.pushButton_5.setStyleSheet("background-color: rgb(55, 159, 147);\n"
"font: 75 10pt \"MS Shell Dlg 2\";")
        self.pushButton_5.setObjectName("pushButton_5")
        self.label_6 = QtWidgets.QLabel(CalcularValveGlobo)
        self.label_6.setGeometry(QtCore.QRect(230, 60, 101, 20))
        font = QtGui.QFont()
        font.setFamily("Calibri")
        font.setPointSize(10)
        self.label_6.setFont(font)
        self.label_6.setObjectName("label_6")
        self.lineEdit_5 = QtWidgets.QLineEdit(CalcularValveGlobo)
        self.lineEdit_5.setGeometry(QtCore.QRect(320, 60, 61, 22))
        self.lineEdit_5.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.lineEdit_5.setObjectName("lineEdit_5")
        self.pushButton_11 = QtWidgets.QPushButton(CalcularValveGlobo)
        self.pushButton_11.setGeometry(QtCore.QRect(400, 90, 61, 21))
        self.pushButton_11.setStyleSheet("color: rgb(255, 255, 255);\n"
"background-color: rgb(82, 52, 62);")
        self.pushButton_11.setObjectName("pushButton_11")
        self.label_11 = QtWidgets.QLabel(CalcularValveGlobo)
        self.label_11.setGeometry(QtCore.QRect(260, 190, 51, 22))
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.label_11.setFont(font)
        self.label_11.setAlignment(QtCore.Qt.AlignCenter)
        self.label_11.setObjectName("label_11")
        self.lineEdit_6 = QtWidgets.QLineEdit(CalcularValveGlobo)
        self.lineEdit_6.setGeometry(QtCore.QRect(310, 190, 61, 22))
        self.lineEdit_6.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.lineEdit_6.setObjectName("lineEdit_6")
        self.label_7 = QtWidgets.QLabel(CalcularValveGlobo)
        self.label_7.setGeometry(QtCore.QRect(260, 100, 31, 21))
        font = QtGui.QFont()
        font.setFamily("Calibri")
        font.setPointSize(10)
        self.label_7.setFont(font)
        self.label_7.setObjectName("label_7")
        self.lineEdit_7 = QtWidgets.QLineEdit(CalcularValveGlobo)
        self.lineEdit_7.setGeometry(QtCore.QRect(320, 100, 61, 22))
        self.lineEdit_7.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.lineEdit_7.setObjectName("lineEdit_7")
        self.lineEdit_8 = QtWidgets.QLineEdit(CalcularValveGlobo)
        self.lineEdit_8.setGeometry(QtCore.QRect(320, 140, 61, 22))
        self.lineEdit_8.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.lineEdit_8.setObjectName("lineEdit_8")
        self.label_8 = QtWidgets.QLabel(CalcularValveGlobo)
        self.label_8.setGeometry(QtCore.QRect(250, 140, 51, 20))
        font = QtGui.QFont()
        font.setFamily("Calibri")
        font.setPointSize(10)
        self.label_8.setFont(font)
        self.label_8.setObjectName("label_8")
        self.label_5 = QtWidgets.QLabel(CalcularValveGlobo)
        self.label_5.setGeometry(QtCore.QRect(20, 20, 51, 16))
        font = QtGui.QFont()
        font.setFamily("Calibri")
        font.setPointSize(10)
        self.label_5.setFont(font)
        self.label_5.setObjectName("label_5")
        self.label_9 = QtWidgets.QLabel(CalcularValveGlobo)
        self.label_9.setGeometry(QtCore.QRect(50, 90, 126, 88))
        self.label_9.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.label_9.setObjectName("label_9")
        self.pushButton_17 = QtWidgets.QPushButton(CalcularValveGlobo)
        self.pushButton_17.setGeometry(QtCore.QRect(300, 250, 91, 28))
        self.pushButton_17.setStyleSheet("color: rgb(255, 255, 255);\n"
"background-color: rgb(82, 52, 62);")
        self.pushButton_17.setObjectName("pushButton_17")
        self.pushButton_18 = QtWidgets.QPushButton(CalcularValveGlobo)
        self.pushButton_18.setGeometry(QtCore.QRect(190, 250, 91, 28))
        self.pushButton_18.setStyleSheet("color: rgb(255, 255, 255);\n"
"background-color: rgb(82, 52, 62);")
        self.pushButton_18.setObjectName("pushButton_18")

        self.retranslateUi(CalcularValveGlobo)
        QtCore.QMetaObject.connectSlotsByName(CalcularValveGlobo)

    def retranslateUi(self, CalcularValveGlobo):
        _translate = QtCore.QCoreApplication.translate
        CalcularValveGlobo.setWindowTitle(_translate("CalcularValveGlobo", "Dialog"))
        self.label_3.setText(_translate("CalcularValveGlobo", "Calcular Válvula Globo"))
        self.pushButton_5.setText(_translate("CalcularValveGlobo", "?"))
        self.label_6.setText(_translate("CalcularValveGlobo", "Ft Material"))
        self.pushButton_11.setText(_translate("CalcularValveGlobo", "Calcular"))
        self.label_11.setText(_translate("CalcularValveGlobo", "K"))
        self.label_7.setText(_translate("CalcularValveGlobo", "∅"))
        self.label_8.setText(_translate("CalcularValveGlobo", "D1/D2"))
        self.label_5.setText(_translate("CalcularValveGlobo", "Válvula"))
        self.label_9.setText(_translate("CalcularValveGlobo", "<html><head/><body><p><img src=\":/Valve/ValveGloboIcon.png\"/></p></body></html>"))
        self.pushButton_17.setText(_translate("CalcularValveGlobo", "Añadir"))
        self.pushButton_18.setText(_translate("CalcularValveGlobo", "Cancelar"))

import mainqrc_rc

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    CalcularValveGlobo = QtWidgets.QDialog()
    ui = Ui_CalcularValveGlobo()
    ui.setupUi(CalcularValveGlobo)
    CalcularValveGlobo.show()
    sys.exit(app.exec_())

