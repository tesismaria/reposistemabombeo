# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '.\Main.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_mainWindow(object):
    def setupUi(self, mainWindow):
        mainWindow.setObjectName("mainWindow")
        mainWindow.resize(993, 753)
        mainWindow.setStyleSheet("background-color: rgb(246, 255, 205);\n"
"border-color: rgb(0, 0, 0);\n"
"")
        self.centralwidget = QtWidgets.QWidget(mainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setGeometry(QtCore.QRect(410, 580, 141, 51))
        self.pushButton.setStyleSheet("background-color: rgb(79, 50, 59);\n"
"color: rgb(246, 255, 205)")
        self.pushButton.setObjectName("pushButton")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(180, 90, 511, 16))
        font = QtGui.QFont()
        font.setFamily("Century Gothic")
        font.setPointSize(12)
        self.label_2.setFont(font)
        self.label_2.setStyleSheet("background-color: rgb(55, 159, 147);")
        self.label_2.setObjectName("label_2")
        self.pushButton_2 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_2.setGeometry(QtCore.QRect(130, 350, 111, 41))
        self.pushButton_2.setAutoFillBackground(False)
        self.pushButton_2.setStyleSheet("background-color: rgb(79, 50, 59);\n"
"border-left-color: rgb(170, 0, 127);\n"
"border-bottom-color: rgb(170, 0, 255);\n"
"color: rgb(246, 255, 205)")
        self.pushButton_2.setObjectName("pushButton_2")
        self.pushButton_3 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_3.setGeometry(QtCore.QRect(680, 350, 141, 51))
        self.pushButton_3.setStyleSheet("background-color: rgb(79, 50, 59);\n"
"color: rgb(246, 255, 205)")
        self.pushButton_3.setObjectName("pushButton_3")
        self.pushButton_4 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_4.setGeometry(QtCore.QRect(400, 350, 141, 51))
        self.pushButton_4.setStyleSheet("color: rgb(246, 255, 205);\n"
"background-color: rgb(79, 50, 59);")
        self.pushButton_4.setObjectName("pushButton_4")
        self.pushButton_5 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_5.setGeometry(QtCore.QRect(120, 580, 141, 51))
        self.pushButton_5.setStyleSheet("background-color: rgb(79, 50, 59);\n"
"color: rgb(246, 255, 205)")
        self.pushButton_5.setObjectName("pushButton_5")
        self.btnQuit = QtWidgets.QPushButton(self.centralwidget)
        self.btnQuit.setGeometry(QtCore.QRect(720, 550, 131, 28))
        self.btnQuit.setMinimumSize(QtCore.QSize(0, 28))
        font = QtGui.QFont()
        font.setPointSize(9)
        font.setBold(True)
        font.setWeight(75)
        self.btnQuit.setFont(font)
        self.btnQuit.setAcceptDrops(False)
        self.btnQuit.setStyleSheet("color: rgb(0, 0, 0);\n"
"background-color: rgb(55, 159, 147);")
        self.btnQuit.setObjectName("btnQuit")
        self.lblLogoUsb = QtWidgets.QLabel(self.centralwidget)
        self.lblLogoUsb.setGeometry(QtCore.QRect(710, 0, 281, 120))
        self.lblLogoUsb.setStyleSheet("background-color: rgb(55, 159, 147);")
        self.lblLogoUsb.setObjectName("lblLogoUsb")
        self.lblTittle1 = QtWidgets.QLabel(self.centralwidget)
        self.lblTittle1.setGeometry(QtCore.QRect(170, 10, 501, 91))
        font = QtGui.QFont()
        font.setFamily("Century Gothic")
        font.setPointSize(20)
        font.setBold(True)
        font.setUnderline(True)
        font.setWeight(75)
        self.lblTittle1.setFont(font)
        self.lblTittle1.setStyleSheet("background-color: rgb(55, 159, 147);")
        self.lblTittle1.setObjectName("lblTittle1")
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setGeometry(QtCore.QRect(20, 0, 131, 121))
        self.label_4.setStyleSheet("background-color: rgb(55, 159, 147);")
        self.label_4.setObjectName("label_4")
        self.btnQuit_2 = QtWidgets.QPushButton(self.centralwidget)
        self.btnQuit_2.setGeometry(QtCore.QRect(720, 480, 131, 51))
        self.btnQuit_2.setMinimumSize(QtCore.QSize(0, 28))
        font = QtGui.QFont()
        font.setPointSize(9)
        font.setBold(True)
        font.setWeight(75)
        self.btnQuit_2.setFont(font)
        self.btnQuit_2.setAcceptDrops(False)
        self.btnQuit_2.setStyleSheet("color: rgb(0, 0, 0);\n"
"background-color: rgb(188, 255, 168);")
        self.btnQuit_2.setObjectName("btnQuit_2")
        self.label_5 = QtWidgets.QLabel(self.centralwidget)
        self.label_5.setGeometry(QtCore.QRect(380, 220, 195, 119))
        self.label_5.setStyleSheet("border-color: rgb(0, 0, 0);\n"
"background-color: rgb(255, 255, 255);")
        self.label_5.setObjectName("label_5")
        self.label_6 = QtWidgets.QLabel(self.centralwidget)
        self.label_6.setGeometry(QtCore.QRect(650, 220, 207, 121))
        self.label_6.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.label_6.setObjectName("label_6")
        self.label_7 = QtWidgets.QLabel(self.centralwidget)
        self.label_7.setGeometry(QtCore.QRect(80, 440, 208, 119))
        self.label_7.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.label_7.setObjectName("label_7")
        self.label_8 = QtWidgets.QLabel(self.centralwidget)
        self.label_8.setGeometry(QtCore.QRect(370, 440, 214, 125))
        self.label_8.setStyleSheet("background-color: rgb(255, 255, 255);\n"
"")
        self.label_8.setObjectName("label_8")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(90, 230, 198, 107))
        self.label.setStyleSheet("border-color: rgb(0, 0, 0);\n"
"selection-color: rgb(255, 170, 255);\n"
"background-color: rgb(255, 255, 255);")
        self.label.setObjectName("label")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(10, 670, 891, 21))
        font = QtGui.QFont()
        font.setFamily("Century Gothic")
        font.setPointSize(10)
        self.label_3.setFont(font)
        self.label_3.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.label_3.setStyleSheet("border-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(0, 0, 0, 255), stop:1 rgba(255, 255, 255, 255));")
        self.label_3.setObjectName("label_3")
        self.label_9 = QtWidgets.QLabel(self.centralwidget)
        self.label_9.setGeometry(QtCore.QRect(0, 0, 1021, 141))
        self.label_9.setStyleSheet("background-color: rgb(55, 159, 147);")
        self.label_9.setText("")
        self.label_9.setObjectName("label_9")
        self.label_9.raise_()
        self.pushButton.raise_()
        self.pushButton_2.raise_()
        self.pushButton_3.raise_()
        self.pushButton_4.raise_()
        self.pushButton_5.raise_()
        self.btnQuit.raise_()
        self.lblLogoUsb.raise_()
        self.lblTittle1.raise_()
        self.label_4.raise_()
        self.label_2.raise_()
        self.btnQuit_2.raise_()
        self.label_5.raise_()
        self.label_6.raise_()
        self.label_7.raise_()
        self.label_8.raise_()
        self.label.raise_()
        self.label_3.raise_()
        mainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(mainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 993, 26))
        self.menubar.setObjectName("menubar")
        mainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(mainWindow)
        self.statusbar.setObjectName("statusbar")
        mainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(mainWindow)
        QtCore.QMetaObject.connectSlotsByName(mainWindow)

    def retranslateUi(self, mainWindow):
        _translate = QtCore.QCoreApplication.translate
        mainWindow.setWindowTitle(_translate("mainWindow", "Sistema Bombeo"))
        self.pushButton.setToolTip(_translate("mainWindow", "<html><head/><body><p><span style=\" font-weight:600; color:#266f66;\">Arreglo de Bombas</span></p><p><span style=\" font-weight:600; color:#266f66;\">Ramales y Dispositivos que Producen Pérdidas</span></p></body></html>"))
        self.pushButton.setText(_translate("mainWindow", "Sistema Complejo"))
        self.label_2.setText(_translate("mainWindow", "Seleccione el tipo de sistema que desea calcular:"))
        self.pushButton_2.setToolTip(_translate("mainWindow", "<html><head/><body><p><span style=\" font-weight:600; color:#2a7c72;\">Una Bomba o Bombas en Serie</span></p><p><span style=\" font-weight:600; color:#2a7c72;\">Un Tanque de Descarga</span></p></body></html>"))
        self.pushButton_2.setText(_translate("mainWindow", " Sistema \n"
" Simple Abierto"))
        self.pushButton_3.setToolTip(_translate("mainWindow", "<html><head/><body><p><span style=\" color:#2b7d73;\">Arreglo de Bombas</span></p><p><span style=\" color:#2b7d73;\">Descarga Múltiple</span></p></body></html>"))
        self.pushButton_3.setText(_translate("mainWindow", " Arreglo Bombas\n"
" Descarga Mútiple "))
        self.pushButton_4.setToolTip(_translate("mainWindow", "<html><head/><body><p><span style=\" font-weight:600; color:#2a7a71;\">Múltiples Bombas en Paralelo</span></p><p><span style=\" font-weight:600; color:#2a7a71;\">Un tanque de Descarga</span></p></body></html>"))
        self.pushButton_4.setText(_translate("mainWindow", "Bombas en Paralelo \n"
"1 Tanque de Descarga "))
        self.pushButton_5.setToolTip(_translate("mainWindow", "<html><head/><body><p><span style=\" font-weight:600; color:#28766d;\">Arreglo de Bombas</span></p><p><span style=\" font-weight:600; color:#28766d;\">Sistema Cerrado</span></p></body></html>"))
        self.pushButton_5.setText(_translate("mainWindow", "Sistema Simple \n"
" Cerrado"))
        self.btnQuit.setText(_translate("mainWindow", "Salir"))
        self.lblLogoUsb.setText(_translate("mainWindow", "<html><head/><body><p><img src=\":/LogoUsb/Logo_USB.png\"/></p></body></html>"))
        self.lblTittle1.setText(_translate("mainWindow", "Cálculos Sistemas de Bombeo"))
        self.label_4.setText(_translate("mainWindow", "<html><head/><body><p><img src=\":/PumpIcon/PumpYB.png\"/></p></body></html>"))
        self.btnQuit_2.setText(_translate("mainWindow", "Ayuda"))
        self.label_5.setText(_translate("mainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><img src=\":/PumpIcon/Diagrama2Icon.png\" /></p></body></html>"))
        self.label_6.setText(_translate("mainWindow", "<html><head/><body><p><img src=\":/Diagramas/Diagrama3Icon.png\"/></p></body></html>"))
        self.label_7.setText(_translate("mainWindow", "<html><head/><body><p><img src=\":/Diagramas/Diagrama4Icon.png\"/></p></body></html>"))
        self.label_8.setText(_translate("mainWindow", "<html><head/><body><p><img src=\":/Diagramas/Diagrama5Icon.png\"/></p></body></html>"))
        self.label.setText(_translate("mainWindow", "<html><head/><body><p><img src=\":/Diagramas/Diagrama1Icon.png\"/></p></body></html>"))
        self.label_3.setText(_translate("mainWindow", "Coordinación de Ingeniería Mecánica, Proyecto de Grado María Díaz 1010209, Tutor Prof. Nathaly Moreno"))



if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    mainWindow = QtWidgets.QMainWindow()
    ui = Ui_mainWindow()
    ui.setupUi(mainWindow)
    mainWindow.show()
    sys.exit(app.exec_())

