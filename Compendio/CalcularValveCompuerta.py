# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '.\CalcularValeCompuerta.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_CalcularValeCompuerta(object):
    def setupUi(self, CalcularValeCompuerta):
        CalcularValeCompuerta.setObjectName("CalcularValeCompuerta")
        CalcularValeCompuerta.resize(542, 291)
        CalcularValeCompuerta.setStyleSheet("background-color: rgb(188, 255, 168);")
        self.label_3 = QtWidgets.QLabel(CalcularValeCompuerta)
        self.label_3.setGeometry(QtCore.QRect(130, 0, 281, 41))
        font = QtGui.QFont()
        font.setFamily("Century Gothic")
        font.setPointSize(12)
        font.setBold(True)
        font.setItalic(False)
        font.setUnderline(True)
        font.setWeight(75)
        self.label_3.setFont(font)
        self.label_3.setStyleSheet("font: 75 bold 12pt \"Century Gothic\";\n"
"text-decoration: underline;")
        self.label_3.setObjectName("label_3")
        self.lineEdit_4 = QtWidgets.QLineEdit(CalcularValeCompuerta)
        self.lineEdit_4.setGeometry(QtCore.QRect(80, 20, 31, 22))
        self.lineEdit_4.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.lineEdit_4.setObjectName("lineEdit_4")
        self.pushButton_5 = QtWidgets.QPushButton(CalcularValeCompuerta)
        self.pushButton_5.setGeometry(QtCore.QRect(470, 10, 31, 28))
        self.pushButton_5.setStyleSheet("background-color: rgb(55, 159, 147);\n"
"font: 75 10pt \"MS Shell Dlg 2\";")
        self.pushButton_5.setObjectName("pushButton_5")
        self.label_6 = QtWidgets.QLabel(CalcularValeCompuerta)
        self.label_6.setGeometry(QtCore.QRect(280, 60, 101, 20))
        font = QtGui.QFont()
        font.setFamily("Calibri")
        font.setPointSize(10)
        self.label_6.setFont(font)
        self.label_6.setObjectName("label_6")
        self.lineEdit_5 = QtWidgets.QLineEdit(CalcularValeCompuerta)
        self.lineEdit_5.setGeometry(QtCore.QRect(370, 60, 61, 22))
        self.lineEdit_5.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.lineEdit_5.setObjectName("lineEdit_5")
        self.pushButton_11 = QtWidgets.QPushButton(CalcularValeCompuerta)
        self.pushButton_11.setGeometry(QtCore.QRect(450, 90, 61, 21))
        self.pushButton_11.setStyleSheet("color: rgb(255, 255, 255);\n"
"background-color: rgb(82, 52, 62);")
        self.pushButton_11.setObjectName("pushButton_11")
        self.label_11 = QtWidgets.QLabel(CalcularValeCompuerta)
        self.label_11.setGeometry(QtCore.QRect(310, 190, 51, 22))
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.label_11.setFont(font)
        self.label_11.setAlignment(QtCore.Qt.AlignCenter)
        self.label_11.setObjectName("label_11")
        self.lineEdit_6 = QtWidgets.QLineEdit(CalcularValeCompuerta)
        self.lineEdit_6.setGeometry(QtCore.QRect(360, 190, 61, 22))
        self.lineEdit_6.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.lineEdit_6.setObjectName("lineEdit_6")
        self.label_7 = QtWidgets.QLabel(CalcularValeCompuerta)
        self.label_7.setGeometry(QtCore.QRect(310, 100, 21, 20))
        font = QtGui.QFont()
        font.setFamily("Calibri")
        font.setPointSize(10)
        self.label_7.setFont(font)
        self.label_7.setObjectName("label_7")
        self.lineEdit_7 = QtWidgets.QLineEdit(CalcularValeCompuerta)
        self.lineEdit_7.setGeometry(QtCore.QRect(370, 100, 61, 22))
        self.lineEdit_7.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.lineEdit_7.setObjectName("lineEdit_7")
        self.lineEdit_8 = QtWidgets.QLineEdit(CalcularValeCompuerta)
        self.lineEdit_8.setGeometry(QtCore.QRect(370, 140, 61, 22))
        self.lineEdit_8.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.lineEdit_8.setObjectName("lineEdit_8")
        self.label_8 = QtWidgets.QLabel(CalcularValeCompuerta)
        self.label_8.setGeometry(QtCore.QRect(300, 140, 51, 20))
        font = QtGui.QFont()
        font.setFamily("Calibri")
        font.setPointSize(10)
        self.label_8.setFont(font)
        self.label_8.setObjectName("label_8")
        self.label_5 = QtWidgets.QLabel(CalcularValeCompuerta)
        self.label_5.setGeometry(QtCore.QRect(20, 20, 51, 16))
        font = QtGui.QFont()
        font.setFamily("Calibri")
        font.setPointSize(10)
        self.label_5.setFont(font)
        self.label_5.setObjectName("label_5")
        self.label = QtWidgets.QLabel(CalcularValeCompuerta)
        self.label.setGeometry(QtCore.QRect(20, 90, 227, 94))
        self.label.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.label.setObjectName("label")
        self.pushButton_17 = QtWidgets.QPushButton(CalcularValeCompuerta)
        self.pushButton_17.setGeometry(QtCore.QRect(270, 240, 91, 28))
        self.pushButton_17.setStyleSheet("color: rgb(255, 255, 255);\n"
"background-color: rgb(82, 52, 62);")
        self.pushButton_17.setObjectName("pushButton_17")
        self.pushButton_18 = QtWidgets.QPushButton(CalcularValeCompuerta)
        self.pushButton_18.setGeometry(QtCore.QRect(160, 240, 91, 28))
        self.pushButton_18.setStyleSheet("color: rgb(255, 255, 255);\n"
"background-color: rgb(82, 52, 62);")
        self.pushButton_18.setObjectName("pushButton_18")

        self.retranslateUi(CalcularValeCompuerta)
        QtCore.QMetaObject.connectSlotsByName(CalcularValeCompuerta)

    def retranslateUi(self, CalcularValeCompuerta):
        _translate = QtCore.QCoreApplication.translate
        CalcularValeCompuerta.setWindowTitle(_translate("CalcularValeCompuerta", "Dialog"))
        self.label_3.setText(_translate("CalcularValeCompuerta", "Calcular Válvula Compuerta"))
        self.pushButton_5.setText(_translate("CalcularValeCompuerta", "?"))
        self.label_6.setText(_translate("CalcularValeCompuerta", "Ft Material"))
        self.pushButton_11.setText(_translate("CalcularValeCompuerta", "Calcular"))
        self.label_11.setText(_translate("CalcularValeCompuerta", "K"))
        self.label_7.setText(_translate("CalcularValeCompuerta", "α"))
        self.label_8.setText(_translate("CalcularValeCompuerta", "D1/D2"))
        self.label_5.setText(_translate("CalcularValeCompuerta", "Válvula"))
        self.label.setText(_translate("CalcularValeCompuerta", "<html><head/><body><p><img src=\":/Valve/ValveCompuertaIcon.png\"/></p></body></html>"))
        self.pushButton_17.setText(_translate("CalcularValeCompuerta", "Añadir"))
        self.pushButton_18.setText(_translate("CalcularValeCompuerta", "Cancelar"))

import mainqrc_rc

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    CalcularValeCompuerta = QtWidgets.QDialog()
    ui = Ui_CalcularValeCompuerta()
    ui.setupUi(CalcularValeCompuerta)
    CalcularValeCompuerta.show()
    sys.exit(app.exec_())

