# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '.\AnadirValvulaK.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_AnadirValvulaK(object):
    def setupUi(self, AnadirValvulaK):
        AnadirValvulaK.setObjectName("AnadirValvulaK")
        AnadirValvulaK.resize(571, 282)
        AnadirValvulaK.setStyleSheet("background-color: rgb(188, 255, 168);")
        self.label_11 = QtWidgets.QLabel(AnadirValvulaK)
        self.label_11.setGeometry(QtCore.QRect(110, 110, 79, 22))
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.label_11.setFont(font)
        self.label_11.setAlignment(QtCore.Qt.AlignCenter)
        self.label_11.setObjectName("label_11")
        self.doubleSpinBox = QtWidgets.QDoubleSpinBox(AnadirValvulaK)
        self.doubleSpinBox.setGeometry(QtCore.QRect(100, 140, 107, 22))
        self.doubleSpinBox.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.doubleSpinBox.setDecimals(4)
        self.doubleSpinBox.setMaximum(1000000.99)
        self.doubleSpinBox.setObjectName("doubleSpinBox")
        self.label_3 = QtWidgets.QLabel(AnadirValvulaK)
        self.label_3.setGeometry(QtCore.QRect(210, 10, 171, 41))
        font = QtGui.QFont()
        font.setFamily("Calibri")
        font.setPointSize(16)
        font.setBold(True)
        font.setUnderline(True)
        font.setWeight(75)
        self.label_3.setFont(font)
        self.label_3.setObjectName("label_3")
        self.label_4 = QtWidgets.QLabel(AnadirValvulaK)
        self.label_4.setGeometry(QtCore.QRect(110, 80, 91, 16))
        font = QtGui.QFont()
        font.setFamily("Calibri")
        font.setPointSize(10)
        self.label_4.setFont(font)
        self.label_4.setObjectName("label_4")
        self.pushButton_11 = QtWidgets.QPushButton(AnadirValvulaK)
        self.pushButton_11.setGeometry(QtCore.QRect(360, 130, 61, 28))
        self.pushButton_11.setStyleSheet("color: rgb(255, 255, 255);\n"
"background-color: rgb(82, 52, 62);")
        self.pushButton_11.setObjectName("pushButton_11")
        self.Anadir = QtWidgets.QDialogButtonBox(AnadirValvulaK)
        self.Anadir.setGeometry(QtCore.QRect(160, 200, 211, 32))
        self.Anadir.setStyleSheet("background-color: rgb(55, 159, 147);")
        self.Anadir.setOrientation(QtCore.Qt.Horizontal)
        self.Anadir.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.Anadir.setObjectName("Anadir")
        self.pushButton_5 = QtWidgets.QPushButton(AnadirValvulaK)
        self.pushButton_5.setGeometry(QtCore.QRect(520, 20, 31, 28))
        self.pushButton_5.setStyleSheet("background-color: rgb(55, 159, 147);\n"
"font: 75 10pt \"MS Shell Dlg 2\";")
        self.pushButton_5.setObjectName("pushButton_5")
        self.label_5 = QtWidgets.QLabel(AnadirValvulaK)
        self.label_5.setGeometry(QtCore.QRect(330, 80, 121, 41))
        font = QtGui.QFont()
        font.setFamily("Calibri")
        font.setPointSize(10)
        self.label_5.setFont(font)
        self.label_5.setObjectName("label_5")
        self.label_6 = QtWidgets.QLabel(AnadirValvulaK)
        self.label_6.setGeometry(QtCore.QRect(220, 50, 61, 20))
        font = QtGui.QFont()
        font.setFamily("Calibri")
        font.setPointSize(10)
        self.label_6.setFont(font)
        self.label_6.setObjectName("label_6")
        self.lineEdit_4 = QtWidgets.QLineEdit(AnadirValvulaK)
        self.lineEdit_4.setGeometry(QtCore.QRect(290, 50, 31, 22))
        self.lineEdit_4.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.lineEdit_4.setObjectName("lineEdit_4")

        self.retranslateUi(AnadirValvulaK)
        QtCore.QMetaObject.connectSlotsByName(AnadirValvulaK)

    def retranslateUi(self, AnadirValvulaK):
        _translate = QtCore.QCoreApplication.translate
        AnadirValvulaK.setWindowTitle(_translate("AnadirValvulaK", "Dialog"))
        self.label_11.setText(_translate("AnadirValvulaK", "K"))
        self.label_3.setText(_translate("AnadirValvulaK", "Añadir Válvula"))
        self.label_4.setText(_translate("AnadirValvulaK", "Introduzca K"))
        self.pushButton_11.setText(_translate("AnadirValvulaK", "Calcular"))
        self.pushButton_5.setText(_translate("AnadirValvulaK", "?"))
        self.label_5.setText(_translate("AnadirValvulaK", "O Intruduzca las \n"
" Caracteristicas"))
        self.label_6.setText(_translate("AnadirValvulaK", "Valvula"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    AnadirValvulaK = QtWidgets.QDialog()
    ui = Ui_AnadirValvulaK()
    ui.setupUi(AnadirValvulaK)
    AnadirValvulaK.show()
    sys.exit(app.exec_())

