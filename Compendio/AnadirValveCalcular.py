# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '.\AnadirValveCalcular.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_AnadirValveCalcular(object):
    def setupUi(self, AnadirValveCalcular):
        AnadirValveCalcular.setObjectName("AnadirValveCalcular")
        AnadirValveCalcular.resize(756, 298)
        AnadirValveCalcular.setStyleSheet("background-color: rgb(188, 255, 168);")
        self.label_3 = QtWidgets.QLabel(AnadirValveCalcular)
        self.label_3.setGeometry(QtCore.QRect(300, 0, 221, 41))
        font = QtGui.QFont()
        font.setFamily("Century Gothic")
        font.setPointSize(12)
        font.setBold(True)
        font.setItalic(False)
        font.setUnderline(True)
        font.setWeight(75)
        self.label_3.setFont(font)
        self.label_3.setStyleSheet("font: 75 bold 12pt \"Century Gothic\";\n"
"text-decoration: underline;")
        self.label_3.setObjectName("label_3")
        self.label_4 = QtWidgets.QLabel(AnadirValveCalcular)
        self.label_4.setGeometry(QtCore.QRect(320, 50, 181, 16))
        font = QtGui.QFont()
        font.setFamily("Calibri")
        font.setPointSize(10)
        self.label_4.setFont(font)
        self.label_4.setObjectName("label_4")
        self.lineEdit_4 = QtWidgets.QLineEdit(AnadirValveCalcular)
        self.lineEdit_4.setGeometry(QtCore.QRect(90, 20, 31, 22))
        self.lineEdit_4.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.lineEdit_4.setObjectName("lineEdit_4")
        self.pushButton_12 = QtWidgets.QPushButton(AnadirValveCalcular)
        self.pushButton_12.setGeometry(QtCore.QRect(90, 200, 91, 28))
        self.pushButton_12.setStyleSheet("color: rgb(255, 255, 255);\n"
"background-color: rgb(82, 52, 62);")
        self.pushButton_12.setObjectName("pushButton_12")
        self.pushButton_5 = QtWidgets.QPushButton(AnadirValveCalcular)
        self.pushButton_5.setGeometry(QtCore.QRect(690, 20, 31, 28))
        self.pushButton_5.setStyleSheet("background-color: rgb(55, 159, 147);\n"
"font: 75 10pt \"MS Shell Dlg 2\";")
        self.pushButton_5.setObjectName("pushButton_5")
        self.label_5 = QtWidgets.QLabel(AnadirValveCalcular)
        self.label_5.setGeometry(QtCore.QRect(20, 20, 51, 16))
        font = QtGui.QFont()
        font.setFamily("Calibri")
        font.setPointSize(10)
        self.label_5.setFont(font)
        self.label_5.setObjectName("label_5")
        self.pushButton_15 = QtWidgets.QPushButton(AnadirValveCalcular)
        self.pushButton_15.setGeometry(QtCore.QRect(610, 200, 91, 28))
        self.pushButton_15.setStyleSheet("color: rgb(255, 255, 255);\n"
"background-color: rgb(82, 52, 62);")
        self.pushButton_15.setObjectName("pushButton_15")
        self.pushButton_16 = QtWidgets.QPushButton(AnadirValveCalcular)
        self.pushButton_16.setGeometry(QtCore.QRect(290, 200, 91, 28))
        self.pushButton_16.setStyleSheet("color: rgb(255, 255, 255);\n"
"background-color: rgb(82, 52, 62);")
        self.pushButton_16.setObjectName("pushButton_16")
        self.pushButton_17 = QtWidgets.QPushButton(AnadirValveCalcular)
        self.pushButton_17.setGeometry(QtCore.QRect(450, 200, 91, 28))
        self.pushButton_17.setStyleSheet("color: rgb(255, 255, 255);\n"
"background-color: rgb(82, 52, 62);")
        self.pushButton_17.setObjectName("pushButton_17")
        self.label = QtWidgets.QLabel(AnadirValveCalcular)
        self.label.setGeometry(QtCore.QRect(20, 92, 227, 94))
        self.label.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(AnadirValveCalcular)
        self.label_2.setGeometry(QtCore.QRect(270, 92, 128, 89))
        self.label_2.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.label_2.setObjectName("label_2")
        self.label_6 = QtWidgets.QLabel(AnadirValveCalcular)
        self.label_6.setGeometry(QtCore.QRect(430, 92, 126, 88))
        self.label_6.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.label_6.setObjectName("label_6")
        self.label_7 = QtWidgets.QLabel(AnadirValveCalcular)
        self.label_7.setGeometry(QtCore.QRect(570, 92, 167, 87))
        self.label_7.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.label_7.setObjectName("label_7")
        self.pushButton_20 = QtWidgets.QPushButton(AnadirValveCalcular)
        self.pushButton_20.setGeometry(QtCore.QRect(20, 50, 91, 28))
        self.pushButton_20.setStyleSheet("color: rgb(255, 255, 255);\n"
"background-color: rgb(55, 159, 147);")
        self.pushButton_20.setObjectName("pushButton_20")

        self.retranslateUi(AnadirValveCalcular)
        QtCore.QMetaObject.connectSlotsByName(AnadirValveCalcular)

    def retranslateUi(self, AnadirValveCalcular):
        _translate = QtCore.QCoreApplication.translate
        AnadirValveCalcular.setWindowTitle(_translate("AnadirValveCalcular", "Dialog"))
        self.label_3.setText(_translate("AnadirValveCalcular", "Calcular K de Válvula"))
        self.label_4.setText(_translate("AnadirValveCalcular", "Elegir un tipo de Válvula"))
        self.pushButton_12.setText(_translate("AnadirValveCalcular", "Compuerta"))
        self.pushButton_5.setText(_translate("AnadirValveCalcular", "?"))
        self.label_5.setText(_translate("AnadirValveCalcular", "Válvula"))
        self.pushButton_15.setText(_translate("AnadirValveCalcular", "Retención"))
        self.pushButton_16.setText(_translate("AnadirValveCalcular", "Mariposa"))
        self.pushButton_17.setText(_translate("AnadirValveCalcular", "Globo"))
        self.label.setText(_translate("AnadirValveCalcular", "<html><head/><body><p><img src=\":/Valve/ValveCompuertaIcon.png\"/></p></body></html>"))
        self.label_2.setText(_translate("AnadirValveCalcular", "<html><head/><body><p><img src=\":/Valve/ValveMariposaIcon.png\"/></p></body></html>"))
        self.label_6.setText(_translate("AnadirValveCalcular", "<html><head/><body><p><img src=\":/Valve/ValveGloboIcon.png\"/></p></body></html>"))
        self.label_7.setText(_translate("AnadirValveCalcular", "<html><head/><body><p><img src=\":/Valve/ValveRetencionIcon.png\"/></p></body></html>"))
        self.pushButton_20.setText(_translate("AnadirValveCalcular", "Cancelar"))

import mainqrc_rc

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    AnadirValveCalcular = QtWidgets.QDialog()
    ui = Ui_AnadirValveCalcular()
    ui.setupUi(AnadirValveCalcular)
    AnadirValveCalcular.show()
    sys.exit(app.exec_())

